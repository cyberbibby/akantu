.. Akantu documentation master file, created by
   sphinx-quickstart on Fri Apr 17 16:35:46 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Akantu: a FEM library
=====================

.. toctree::
   :maxdepth: 2
   :caption: User Manual

   ./manual/getting_started.rst
   ./manual/fe_engine.rst
   ./manual/solidmechanicsmodel.rst
   ./manual/heattransfermodel.rst
   ./manual/contactmechanicsmodel.rst
   ./manual/phasefieldmodel.rst
   ./manual/structuralmechanicsmodel.rst
   ./manual/io.rst

.. toctree::
   :maxdepth: 2
   :caption: Changelog

   ./changelog.rst

.. toctree::
   :maxdepth: 2
   :caption: API Reference

   ./reference.rst

.. toctree::
   :maxdepth: 2
   :caption: Appendix

   ./manual/appendix.rst

.. toctree::
   :maxdepth: 2
   :caption: Bibliography

   ./manual/bibliography.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
