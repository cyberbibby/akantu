sphinx==3.4.3
sphinx-rtd-theme==0.5.2
sphinxcontrib-bibtex==2.3.0
breathe==4.30.0
jinja2==3.0.1
gitpython==3.1.18
